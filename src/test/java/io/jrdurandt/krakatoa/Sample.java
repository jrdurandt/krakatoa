package io.jrdurandt.krakatoa;

import io.jrdurandt.krakatoa.core.VkWindow;
import io.jrdurandt.krakatoa.vk.Device;
import io.jrdurandt.krakatoa.vk.Instance;
import io.jrdurandt.krakatoa.vk.PhysicalDevice;
import io.jrdurandt.krakatoa.vk.Surface;
import io.jrdurandt.krakatoa.vk.structs.ApplicationInfo;
import io.jrdurandt.krakatoa.vk.structs.QueueFamily;
import io.jrdurandt.krakatoa.vk.structs.Version;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.vulkan.EXTDebugUtils;
import org.lwjgl.vulkan.KHRSwapchain;
import org.tinylog.Logger;

import java.util.List;

public class Sample {
    public Sample() {

    }

    public void run() throws Exception {
        try (var window = new VkWindow("Sample", 800, 600)) {

            var extensions = VkWindow.getRequiredInstanceExtensionNames();
            extensions.add(EXTDebugUtils.VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

            try (var instance = new Instance(
                    new ApplicationInfo("Sample", new Version(1, 0, 0), "Sample", new Version(1, 0, 0)),
                    extensions,
                    List.of("VK_LAYER_KHRONOS_validation"))) {

                var physicalDevice = PhysicalDevice.enumeratePhysicalDevices(instance).stream().findFirst().orElseThrow();

                Logger.debug("Physical Device: {}", physicalDevice.getProperties().deviceNameString());

                try (var surface = new Surface(instance, window, physicalDevice)) {

                    var graphicsQueueFamily = physicalDevice.getQueueFamilies().stream().filter(QueueFamily::graphicsSupported).findFirst().orElseThrow();

                    var presentQueueFamily = physicalDevice.getQueueFamilies().stream().filter(surface::isPresentSupported).findFirst().orElseThrow();

                    try (var device = new Device(physicalDevice,
                            List.of(KHRSwapchain.VK_KHR_SWAPCHAIN_EXTENSION_NAME),
                            List.of())) {

                        var presentQueue = device.getQueue(presentQueueFamily, 0);

                        window.show();
                        while (!window.shouldClose()) {
                            GLFW.glfwPollEvents();

                            if (window.isKeyPressed(GLFW.GLFW_KEY_ESCAPE))
                                window.exit();
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            var sample = new Sample();
            sample.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
