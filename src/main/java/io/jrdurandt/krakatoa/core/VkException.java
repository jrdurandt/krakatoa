package io.jrdurandt.krakatoa.core;

import org.lwjgl.vulkan.VK10;

public final class VkException extends Exception {
    public VkException(int result){
        super(translateResult(result));
    }

    public static void check(int result) throws VkException {
        if(result != VK10.VK_SUCCESS){
            throw new VkException(result);
        }
    }

    public static String translateResult(int result){
        return "Unknown Error: " + result;
    }
}
