package io.jrdurandt.krakatoa.core;

import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFWVulkan;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public final class VkWindow implements AutoCloseable {
    private int width, height;
    private final long handle;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public long getHandle() {
        return handle;
    }

    public VkWindow(String title, int width, int height) {
        this.width = width;
        this.height = height;

        if (!glfwInit()) {
            throw new RuntimeException("Failed to init GLFW");
        }

        if (!GLFWVulkan.glfwVulkanSupported()) {
            throw new RuntimeException("Vulkan not supported");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        handle = glfwCreateWindow(width, height, title, 0, 0);
        if (handle == GLFW_FALSE) {
            throw new RuntimeException("Failed to create GLFW window");
        }
        Logger.debug("Created GLFW window: [{}]", handle);
    }

    public void show() {
        glfwShowWindow(handle);
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(handle);
    }

    public void exit() {
        glfwSetWindowShouldClose(handle, true);
    }

    @Override
    public void close() {
        Logger.debug("Destroyed GLFW window: [{}]", handle);
        Callbacks.glfwFreeCallbacks(handle);
        glfwDestroyWindow(handle);
        glfwTerminate();
    }

    public boolean isKeyPressed(int key) {
        return glfwGetKey(handle, key) == GLFW_PRESS;
    }

    public static List<String> getRequiredInstanceExtensionNames(){
        var pRequiredExtensions = GLFWVulkan.glfwGetRequiredInstanceExtensions();
        if(pRequiredExtensions == null){
            throw new RuntimeException("Failed to determine required instance extensions");
        }

        var requiredExtensionNames = new ArrayList<String>(pRequiredExtensions.capacity());
        while(pRequiredExtensions.hasRemaining()){
            requiredExtensionNames.add(pRequiredExtensions.getStringUTF8());
        }
        return requiredExtensionNames;
    }
}
