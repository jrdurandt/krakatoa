package io.jrdurandt.krakatoa.vk.structs;

import org.lwjgl.vulkan.VkSurfaceFormatKHR;

public record SurfaceFormat(int surfaceFormat, int colorSpace) {
    public static SurfaceFormat from(VkSurfaceFormatKHR surfaceFormat) {
        return new SurfaceFormat(surfaceFormat.format(), surfaceFormat.colorSpace());
    }
}
