package io.jrdurandt.krakatoa.vk.structs;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK13;
import org.lwjgl.vulkan.VkApplicationInfo;

public record ApplicationInfo(String applicationName, Version applicationVersion,
                              String engineName, Version engineVersion) {
    public static int API_VERSION = VK13.VK_API_VERSION_1_3;

    public VkApplicationInfo calloc(MemoryStack stack) {
        return VkApplicationInfo.calloc(stack)
                .sType$Default()
                .pApplicationName(stack.UTF8(applicationName))
                .applicationVersion(applicationVersion.make())
                .pEngineName(stack.UTF8(engineName))
                .engineVersion(engineVersion.make())
                .apiVersion(API_VERSION);
    }
}
