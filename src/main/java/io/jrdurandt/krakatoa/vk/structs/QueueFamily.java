package io.jrdurandt.krakatoa.vk.structs;

public record QueueFamily(int index, int queueCount,
                          boolean graphicsSupported,
                          boolean computeSupported,
                          boolean transferSupported) {
}
