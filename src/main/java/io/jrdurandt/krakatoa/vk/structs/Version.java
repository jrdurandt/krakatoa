package io.jrdurandt.krakatoa.vk.structs;

import org.lwjgl.vulkan.VK10;

public record Version(int major, int minor, int patch) {
    public int make(){
        return VK10.VK_MAKE_VERSION(major, minor, patch);
    }

    public static Version from(int version){
        return new Version(
                VK10.VK_VERSION_MAJOR(version),
                VK10.VK_VERSION_MINOR(version),
                VK10.VK_VERSION_PATCH(version)
        );
    }
}
