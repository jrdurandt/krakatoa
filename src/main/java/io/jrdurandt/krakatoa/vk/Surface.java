package io.jrdurandt.krakatoa.vk;

import io.jrdurandt.krakatoa.core.VkException;
import io.jrdurandt.krakatoa.core.VkWindow;
import io.jrdurandt.krakatoa.vk.structs.QueueFamily;
import io.jrdurandt.krakatoa.vk.structs.SurfaceFormat;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRSurface;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkSurfaceCapabilitiesKHR;
import org.lwjgl.vulkan.VkSurfaceFormatKHR;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public final class Surface implements AutoCloseable {
    private final Instance instance;
    public final PhysicalDevice physicalDevice;
    private Long handle;
    private final VkSurfaceCapabilitiesKHR capabilities;

    private final List<SurfaceFormat> surfaceFormats;
    private final List<Integer> presentModes;

    public Long getHandle() {
        return handle;
    }

    public VkSurfaceCapabilitiesKHR getCapabilities() {
        return capabilities;
    }

    public List<SurfaceFormat> getSurfaceFormats() {
        return surfaceFormats;
    }

    public List<Integer> getPresentModes() {
        return presentModes;
    }

    public Surface(Instance instance, VkWindow window, PhysicalDevice physicalDevice) throws VkException {
        this.instance = instance;
        this.physicalDevice = physicalDevice;

        try (var stack = MemoryStack.stackPush()) {
            var pSurface = stack.mallocLong(1);

            var res = GLFWVulkan.glfwCreateWindowSurface(instance.getHandle(), window.getHandle(), null, pSurface);
            VkException.check(res);

            handle = pSurface.get(0);
            Logger.debug("Created VkSurface: [{}]", handle);
        }

        capabilities = VkSurfaceCapabilitiesKHR.create();
        KHRSurface.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice.getHandle(), handle, capabilities);

        surfaceFormats = retrieveSurfaceFormats();
        presentModes = retrievePresentModes();
    }

    private List<SurfaceFormat> retrieveSurfaceFormats() throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pSurfaceFormatCount = stack.mallocInt(1);
            var res = KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice.getHandle(), handle, pSurfaceFormatCount, null);
            VkException.check(res);

            var count = pSurfaceFormatCount.get(0);
            var pSurfaceFormats = VkSurfaceFormatKHR.calloc(count, stack);
            res = KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice.getHandle(), handle, pSurfaceFormatCount, pSurfaceFormats);
            VkException.check(res);

            var surfaceFormats = new ArrayList<SurfaceFormat>(count);
            while (pSurfaceFormats.hasRemaining()) {
                surfaceFormats.add(SurfaceFormat.from(pSurfaceFormats.get()));
            }
            return surfaceFormats;
        }
    }

    private List<Integer> retrievePresentModes() throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPresentModeCount = stack.mallocInt(1);
            var res = KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice.getHandle(), handle, pPresentModeCount, null);
            VkException.check(res);

            var count = pPresentModeCount.get(0);
            var pPresentModes = stack.mallocInt(count);
            res = KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice.getHandle(), handle, pPresentModeCount, pPresentModes);
            VkException.check(res);

            var presentModes = new ArrayList<Integer>(count);
            while (pPresentModes.hasRemaining()) {
                presentModes.add(pPresentModes.get());
            }
            return presentModes;
        }
    }

    @Override
    public void close() {
        KHRSurface.vkDestroySurfaceKHR(instance.getHandle(), handle, null);
        Logger.debug("Destroyed VkSurface: [{}]", handle);
        handle = null;
    }

    public boolean isPresentSupported(QueueFamily queueFamily) {
        try (var stack = MemoryStack.stackPush()) {
            var pSupported = stack.mallocInt(1);
            var res = KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice.getHandle(), queueFamily.index(), handle, pSupported);
            VkException.check(res);
            return pSupported.get(0) == VK10.VK_TRUE;
        } catch (VkException e) {
            throw new RuntimeException(e);
        }
    }
}
