package io.jrdurandt.krakatoa.vk;

import io.jrdurandt.krakatoa.vk.structs.QueueFamily;
import org.lwjgl.vulkan.VkQueue;

public final class Queue {
    private final QueueFamily queueFamily;
    private final int index;
    private final VkQueue handle;

    public QueueFamily getQueueFamily() {
        return queueFamily;
    }

    public int getIndex() {
        return index;
    }

    public VkQueue getHandle() {
        return handle;
    }

    protected Queue(long address, Device device, QueueFamily queueFamily, int index) {
        this.queueFamily = queueFamily;
        this.index = index;

        handle = new VkQueue(address, device.getHandle());
    }
}
