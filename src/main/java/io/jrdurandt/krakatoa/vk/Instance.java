package io.jrdurandt.krakatoa.vk;

import io.jrdurandt.krakatoa.core.VkException;
import io.jrdurandt.krakatoa.vk.structs.ApplicationInfo;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.*;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public final class Instance implements AutoCloseable {
    public static final int MESSAGE_SEVERITY_BITMASK = EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
            EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
    public static final int MESSAGE_TYPE_BITMASK = EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    private boolean debugEnabled = false;
    private VkDebugUtilsMessengerCreateInfoEXT debugUtils;
    private Long debugMessengerHandle;
    private VkInstance handle;

    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    public VkInstance getHandle() {
        return handle;
    }

    public Instance(ApplicationInfo applicationInfo,
                    List<String> extensionNames,
                    List<String> layerNames) throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            if (extensionNames.contains(EXTDebugUtils.VK_EXT_DEBUG_UTILS_EXTENSION_NAME)) {
                debugEnabled = true;
                Logger.debug("Vulkan validation and debugging enabled");
                debugUtils = createDebugUtilsMessengerCallBack();
            }

            var enabledExtensionNames = enumerateExtensionProperties(null).stream()
                    .map(VkExtensionProperties::extensionNameString)
                    .filter(extensionNames::contains)
                    .toList();
            Logger.debug("Enable instance extension names: [{}]", enabledExtensionNames);
            PointerBuffer pEnabledExtensionNames = null;
            if (!enabledExtensionNames.isEmpty()) {
                pEnabledExtensionNames = MemoryUtil.memCallocPointer(enabledExtensionNames.size());
                enabledExtensionNames.stream()
                        .map(stack::UTF8)
                        .forEach(pEnabledExtensionNames::put);
                pEnabledExtensionNames.flip();
            }

            var enabledLayerNames = enumerateLayerProperties().stream()
                    .map(VkLayerProperties::layerNameString)
                    .filter(layerNames::contains)
                    .toList();
            Logger.debug("Enable instance layer names: [{}]", enabledLayerNames);
            PointerBuffer pEnabledLayerNames = null;
            if (!enabledLayerNames.isEmpty()) {
                pEnabledLayerNames = MemoryUtil.memCallocPointer(enabledLayerNames.size());
                enabledLayerNames.stream()
                        .map(stack::UTF8)
                        .forEach(pEnabledLayerNames::put);
                pEnabledLayerNames.flip();
            }

            var createInfo = VkInstanceCreateInfo.calloc(stack)
                    .sType$Default()
                    .pNext(debugUtils != null ? debugUtils.address() : MemoryUtil.NULL)
                    .pApplicationInfo(applicationInfo.calloc(stack))
                    .ppEnabledExtensionNames(pEnabledExtensionNames)
                    .ppEnabledLayerNames(pEnabledLayerNames);

            var pInstance = stack.mallocPointer(1);
            var res = VK10.vkCreateInstance(createInfo, null, pInstance);
            VkException.check(res);

            handle = new VkInstance(pInstance.get(0), createInfo);
            Logger.debug("Created VkInstance: [{}]", handle.address());

            if (debugEnabled) {
                var pDebugMessenger = stack.mallocLong(1);
                res = EXTDebugUtils.vkCreateDebugUtilsMessengerEXT(handle, debugUtils, null, pDebugMessenger);
                VkException.check(res);
                debugMessengerHandle = pDebugMessenger.get(0);
                Logger.debug("Created VkDebugUtilsMessenger: [{}]", debugMessengerHandle);
            }
        }
    }

    private static VkDebugUtilsMessengerCreateInfoEXT createDebugUtilsMessengerCallBack() {
        return VkDebugUtilsMessengerCreateInfoEXT
                .calloc()
                .sType(EXTDebugUtils.VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT)
                .messageSeverity(MESSAGE_SEVERITY_BITMASK)
                .messageType(MESSAGE_TYPE_BITMASK)
                .pfnUserCallback((messageSeverity, messageTypes, pCallbackData, pUserData) -> {
                    VkDebugUtilsMessengerCallbackDataEXT callbackData = VkDebugUtilsMessengerCallbackDataEXT.create(pCallbackData);
                    if ((messageSeverity & EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) != 0) {
                        Logger.info("VkDebugUtilsCallback, {}", callbackData.pMessageString());
                    } else if ((messageSeverity & EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) != 0) {
                        Logger.warn("VkDebugUtilsCallback, {}", callbackData.pMessageString());
                    } else if ((messageSeverity & EXTDebugUtils.VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) != 0) {
                        Logger.error("VkDebugUtilsCallback, {}", callbackData.pMessageString());
                    } else {
                        Logger.debug("VkDebugUtilsCallback, {}", callbackData.pMessageString());
                    }
                    return VK10.VK_FALSE;
                });
    }

    @Override
    public void close() throws Exception {
        if (debugMessengerHandle != null) {
            EXTDebugUtils.vkDestroyDebugUtilsMessengerEXT(handle, debugMessengerHandle, null);
            Logger.debug("Destroyed VkDebugUtilsMessenger: [{}]", handle);
            debugMessengerHandle = null;
        }

        VK10.vkDestroyInstance(handle, null);
        Logger.debug("Destroyed VkInstance: [{}]", handle.address());
        handle = null;

        if (debugUtils != null) {
            debugUtils.pfnUserCallback().free();
            debugUtils.free();
        }
    }

    public static List<VkLayerProperties> enumerateLayerProperties() throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPropertyCount = stack.mallocInt(1);
            var res = VK10.vkEnumerateInstanceLayerProperties(pPropertyCount, null);
            VkException.check(res);

            var count = pPropertyCount.get(0);
            var pProperties = VkLayerProperties.calloc(count, stack);
            res = VK10.vkEnumerateInstanceLayerProperties(pPropertyCount, pProperties);
            VkException.check(res);

            var properties = new ArrayList<VkLayerProperties>(count);
            while (pProperties.hasRemaining()) {
                properties.add(pProperties.get());
            }
            return properties;
        }
    }

    public static List<VkExtensionProperties> enumerateExtensionProperties(String layerName) throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPropertyCount = stack.mallocInt(1);
            var res = VK10.vkEnumerateInstanceExtensionProperties(layerName, pPropertyCount, null);
            VkException.check(res);

            var count = pPropertyCount.get(0);
            var pProperties = VkExtensionProperties.calloc(count, stack);
            res = VK10.vkEnumerateInstanceExtensionProperties(layerName, pPropertyCount, pProperties);
            VkException.check(res);

            var properties = new ArrayList<VkExtensionProperties>(count);
            while (pProperties.hasRemaining()) {
                properties.add(pProperties.get());
            }
            return properties;
        }
    }
}
