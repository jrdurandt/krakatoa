package io.jrdurandt.krakatoa.vk;

import io.jrdurandt.krakatoa.core.VkException;
import io.jrdurandt.krakatoa.vk.structs.QueueFamily;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.*;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public final class Device implements AutoCloseable {
    private final PhysicalDevice physicalDevice;
    private VkDevice handle;

    public VkDevice getHandle() {
        return handle;
    }

    public Device(PhysicalDevice physicalDevice,
                  List<String> extensionNames,
                  List<String> layerNames) throws VkException {
        this.physicalDevice = physicalDevice;
        try (var stack = MemoryStack.stackPush()) {
            var queueFamilies = physicalDevice.getQueueFamilies();
            var pQueueFamilyInfos = VkDeviceQueueCreateInfo.calloc(queueFamilies.size(), stack);
            var q = 0;
            for (var queueFamily : queueFamilies) {
                pQueueFamilyInfos.get(q++)
                        .sType$Default()
                        .queueFamilyIndex(queueFamily.index())
                        .pQueuePriorities(stack.callocFloat(queueFamily.queueCount()));
            }

            var enabledExtensionNames = physicalDevice.enumerateExtensionProperties(null).stream()
                    .map(VkExtensionProperties::extensionNameString)
                    .filter(extensionNames::contains)
                    .toList();
            Logger.debug("Enable device extension names: [{}]", enabledExtensionNames);
            PointerBuffer pEnabledExtensionNames = null;
            if (!enabledExtensionNames.isEmpty()) {
                pEnabledExtensionNames = MemoryUtil.memCallocPointer(enabledExtensionNames.size());
                enabledExtensionNames.stream()
                        .map(stack::UTF8)
                        .forEach(pEnabledExtensionNames::put);
                pEnabledExtensionNames.flip();
            }

            var enabledLayerNames = physicalDevice.enumerateLayerProperties().stream()
                    .map(VkLayerProperties::layerNameString)
                    .filter(layerNames::contains)
                    .toList();
            Logger.debug("Enable device layer names: [{}]", enabledLayerNames);
            PointerBuffer pEnabledLayerNames = null;
            if (!enabledLayerNames.isEmpty()) {
                pEnabledLayerNames = MemoryUtil.memCallocPointer(enabledLayerNames.size());
                enabledLayerNames.stream()
                        .map(stack::UTF8)
                        .forEach(pEnabledLayerNames::put);
                pEnabledLayerNames.flip();
            }

            var createInfo = VkDeviceCreateInfo.calloc(stack)
                    .sType$Default()
                    .pQueueCreateInfos(pQueueFamilyInfos)
                    .pEnabledFeatures(physicalDevice.getFeatures())
                    .ppEnabledExtensionNames(pEnabledExtensionNames)
                    .ppEnabledLayerNames(pEnabledLayerNames);

            var pDevice = stack.mallocPointer(1);
            var res = VK10.vkCreateDevice(physicalDevice.getHandle(), createInfo, null, pDevice);
            VkException.check(res);

            handle = new VkDevice(pDevice.get(0), physicalDevice.getHandle(), createInfo);
            Logger.debug("Created VkDevice: [{}]", handle.address());
        }
    }

    public Queue getQueue(QueueFamily queueFamily, int queueIndex){
        try (var stack = MemoryStack.stackPush()) {
            var pQueue = stack.mallocPointer(1);
            VK10.vkGetDeviceQueue(handle, queueFamily.index(), queueIndex, pQueue);
            return new Queue(pQueue.get(0), this, queueFamily, queueIndex);
        }
    }

    @Override
    public void close() throws Exception {
        VK10.vkDestroyDevice(handle, null);
        Logger.debug("Destroyed VkDevice: [{}]", handle.address());
        handle = null;
    }
}
