package io.jrdurandt.krakatoa.vk;

import io.jrdurandt.krakatoa.core.VkException;
import io.jrdurandt.krakatoa.vk.structs.QueueFamily;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.util.ArrayList;
import java.util.List;

public final class PhysicalDevice {
    private final VkPhysicalDevice handle;
    private final VkPhysicalDeviceProperties properties;
    private final VkPhysicalDeviceFeatures features;
    private final VkPhysicalDeviceMemoryProperties memoryProperties;
    private final List<QueueFamily> queueFamilies;

    public VkPhysicalDevice getHandle() {
        return handle;
    }

    public VkPhysicalDeviceProperties getProperties() {
        return properties;
    }

    public VkPhysicalDeviceFeatures getFeatures() {
        return features;
    }

    public VkPhysicalDeviceMemoryProperties getMemoryProperties() {
        return memoryProperties;
    }

    public List<QueueFamily> getQueueFamilies() {
        return queueFamilies;
    }

    private PhysicalDevice(long address, VkInstance instance) {
        handle = new VkPhysicalDevice(address, instance);

        properties = VkPhysicalDeviceProperties.create();
        VK10.vkGetPhysicalDeviceProperties(handle, properties);

        features = VkPhysicalDeviceFeatures.create();
        VK10.vkGetPhysicalDeviceFeatures(handle, features);

        memoryProperties = VkPhysicalDeviceMemoryProperties.create();
        VK10.vkGetPhysicalDeviceMemoryProperties(handle, memoryProperties);

        queueFamilies = retrieveQueueFamilies();
    }

    private List<QueueFamily> retrieveQueueFamilies() {
        try (var stack = MemoryStack.stackPush()) {
            var pQueueFamilyPropertyCount = stack.mallocInt(1);
            VK10.vkGetPhysicalDeviceQueueFamilyProperties(handle, pQueueFamilyPropertyCount, null);

            var count = pQueueFamilyPropertyCount.get(0);
            var pQueueFamilyProperties = VkQueueFamilyProperties.calloc(count, stack);
            VK10.vkGetPhysicalDeviceQueueFamilyProperties(handle, pQueueFamilyPropertyCount, pQueueFamilyProperties);

            var queueFamilies = new ArrayList<QueueFamily>();
            for (var i = 0; i < count; i++) {
                var queueFamilyProperty = pQueueFamilyProperties.get(i);
                queueFamilies.add(
                        new QueueFamily(i,
                                queueFamilyProperty.queueCount(),
                                (queueFamilyProperty.queueFlags() & VK10.VK_QUEUE_GRAPHICS_BIT) != 0,
                                (queueFamilyProperty.queueFlags() & VK10.VK_QUEUE_COMPUTE_BIT) != 0,
                                (queueFamilyProperty.queueFlags() & VK10.VK_QUEUE_TRANSFER_BIT) != 0
                        )
                );
            }
            return queueFamilies;
        }
    }

    public List<VkLayerProperties> enumerateLayerProperties() throws VkException {
        return PhysicalDevice.enumerateLayerProperties(this);
    }

    public List<VkExtensionProperties> enumerateExtensionProperties(String layerName) throws VkException {
        return PhysicalDevice.enumerateExtensionProperties(this, layerName);
    }

    public static List<PhysicalDevice> enumeratePhysicalDevices(Instance instance) throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPhysicalDeviceCount = stack.mallocInt(1);
            var res = VK10.vkEnumeratePhysicalDevices(instance.getHandle(), pPhysicalDeviceCount, null);
            VkException.check(res);

            var count = pPhysicalDeviceCount.get(0);
            var pPhysicalDevices = stack.mallocPointer(count);
            res = VK10.vkEnumeratePhysicalDevices(instance.getHandle(), pPhysicalDeviceCount, pPhysicalDevices);
            VkException.check(res);

            var physicalDevices = new ArrayList<PhysicalDevice>(count);
            while (pPhysicalDevices.hasRemaining()) {
                physicalDevices.add(new PhysicalDevice(pPhysicalDevices.get(), instance.getHandle()));
            }
            return physicalDevices;
        }
    }

    public static List<VkLayerProperties> enumerateLayerProperties(PhysicalDevice physicalDevice) throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPropertyCount = stack.mallocInt(1);
            var res = VK10.vkEnumerateDeviceLayerProperties(physicalDevice.getHandle(), pPropertyCount, null);
            VkException.check(res);

            var count = pPropertyCount.get(0);
            var pProperties = VkLayerProperties.calloc(count, stack);
            res = VK10.vkEnumerateDeviceLayerProperties(physicalDevice.getHandle(), pPropertyCount, pProperties);
            VkException.check(res);

            var properties = new ArrayList<VkLayerProperties>(count);
            while (pProperties.hasRemaining()) {
                properties.add(pProperties.get());
            }
            return properties;
        }
    }

    public static List<VkExtensionProperties> enumerateExtensionProperties(PhysicalDevice physicalDevice, String layerName) throws VkException {
        try (var stack = MemoryStack.stackPush()) {
            var pPropertyCount = stack.mallocInt(1);
            var res = VK10.vkEnumerateDeviceExtensionProperties(physicalDevice.getHandle(), layerName, pPropertyCount, null);
            VkException.check(res);

            var count = pPropertyCount.get(0);
            var pProperties = VkExtensionProperties.calloc(count, stack);
            res = VK10.vkEnumerateDeviceExtensionProperties(physicalDevice.getHandle(), layerName, pPropertyCount, pProperties);
            VkException.check(res);

            var properties = new ArrayList<VkExtensionProperties>(count);
            while (pProperties.hasRemaining()) {
                properties.add(pProperties.get());
            }
            return properties;
        }
    }
}
